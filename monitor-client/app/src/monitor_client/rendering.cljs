(ns monitor-client.rendering
  (:require [domina :as dom]
            [io.pedestal.app.render.push :as render]
            [io.pedestal.app.render.push.templates :as templates]
            [io.pedestal.app.render.push.handlers.automatic :as d]
            [io.pedestal.app.render.push.handlers :as h]
            [jayq.core :refer [$]])
  (:require-macros [monitor-client.html-templates :as html-templates]))

(def received-counts-chart (atom nil))

(defn set-running-state [_ [_ _ _ new-value] _]
  (let [text (if new-value "Stop" "Start")]
    (-> ($ :#start_stop_button)
      (.html text))))

(defn prepare-received-counts-chart [_ _ _]
  (reset! received-counts-chart ($/plot 
                 "#received_counts" 
                 (clj->js [])
                 (clj->js {:xaxis { :mode "time"}}))))

(defn render-received-counts-chart [_ [_ _ _ new-value] _]
  (when (not-empty new-value)
    (let [data new-value
          xaxis (get (aget (.getOptions @received-counts-chart) "xaxes") 0)
          [time-end _] (last new-value)
          time-start (- time-end 60000)]
    (aset xaxis "min" time-start)
    (aset xaxis "max" now)
    (.setData @received-counts-chart (clj->js [data]))
    (.setupGrid @received-counts-chart)
    (.draw @received-counts-chart))))

(defn render-config []
  [[:node-create [:received-counts] prepare-received-counts-chart]
   [:value [:received-counts] render-received-counts-chart]
   
   [:transform-enable [:simulation :running] (h/add-send-on-click "start_stop_button")]
   [:value [:simulation :running] set-running-state]])
